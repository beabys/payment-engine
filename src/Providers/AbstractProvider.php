<?php

namespace PaymentEngine\Providers;

/**
 * Class AbstractProvider
 * @package PaymentEngine\Providers
 * @author Alfonso Rodriguez <beabys@gmail.com>
 */
abstract class AbstractProvider implements ProviderInterface
{

    const SUCCESS_PAYMENT = 'success';

    const PENDING_PAYMENT = 'pending';

    const PENDING_CANCEL = 'cancel';

    protected $redirect = null;

    /**
     * @var paymentMethod name
     */
    protected $name = null;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param $message
     */
    public function charge($message)
    {
        // TODO: Implement charge() method.
    }

    /**
     * @return bool
     */
    public function hasRedirect()
    {
        return false;
    }

    /**
     * @return null
     */
    public function getRedirect()
    {
        return $this->redirect;
    }

    /**
     * @param $redirect
     * @return $this
     */
    public function setRedirect($redirect)
    {
        $this->redirect = $redirect;
        return $this;
    }


}
