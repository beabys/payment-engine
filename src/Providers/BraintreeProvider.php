<?php

namespace PaymentEngine\Providers;

use Braintree_Configuration;
use Braintree_Customer;
use Braintree_Transaction;
use PaymentEngine\Messages\Error;
use PaymentEngine\Messages\Success;

/**
 * Class BraintreeProvider
 * @package PaymentEngine\Providers
 * @author Alfonso Rodriguez <beabys@gmail.com>
 */
class BraintreeProvider extends AbstractProvider
{

    protected $name = 'braintree';

    /**
     * BraintreeProvider constructor.
     * @param array $params
     */
    public function __construct(array $params)
    {
        Braintree_Configuration::environment($params['environment']);
        Braintree_Configuration::merchantId($params['merchant_id']);
        Braintree_Configuration::publicKey($params['public_key']);
        Braintree_Configuration::privateKey($params['private_key']);
    }

    /**
     * @param $message
     * @param string $customerId
     * @return Error|Success
     */
    public function charge($message, $customerId = null)
    {
        if (!is_null($customerId)) {
            return $this->customerTransaction($message, $customerId);
        }
        return $this->generalTransaction($message);
    }

    /**
     * @param $message
     * @return Error|Success
     */
    protected function generalTransaction($message)
    {
        list($expMont, $expYear) =  explode('/', $message['credit_expiration']);
        $sale = [
            'firstName' => $message['firstname'],
            'lastName'  => $message['lastname'],
            'phone'     => $message['phone'],
            'email'     => $message['email'],
            'creditCard' => [
                'number'          => implode('', explode('-', $message['credit_number'])),
                'cardholderName'  => $message['holder_name'],
                'expirationMonth' => $expMont,
                'expirationYear'  => $expYear,
                'cvv'             => $message['cvv'],
            ]
        ];
        $result = Braintree_Customer::create($sale);
        if ($result->success) {
            return $this->customerTransaction($message, $result->customer->id);
        }
        return new Error("Error : " . $result->message);

    }

    /**
     * @param $message
     * @param $customerId
     * @return Error|Success
     */
    protected function customerTransaction($message, $customerId)
    {
        $price = isset($message['currency_base']) && isset($message['price_in_' . $message['currency_base']]) ?
            $message['price_in_' . $message['currency_base']] :
            $message['price'];
        $sale = [
            'customerId' => $customerId,
            'amount'   => $price,
            'orderId'  => isset($message['invoiceid']) ? $message['invoiceid'] : $this->getInvoiceId(),
            'options' => array('submitForSettlement'   => true)
        ];

        $result = Braintree_Transaction::sale($sale);
        $transaction = (array) $result->transaction;
        $transaction = json_decode(json_encode(array_shift($transaction)), true);
        if ($result->success)
        {
            $success = new Success();
            return $success
                ->setCustomerId($customerId)
                ->setPaymentResult($transaction)
                ->setTransactionId($result->transaction->id)
            ;
        }
        $error = new Error("Error : " . $result->_attributes['message']);
        return $error->setCustomerId($customerId)
            ->setPaymentResult($transaction)
        ;
    }

    /**
     * @return string
     */
    protected function getInvoiceId()
    {
        return uniqid();
    }
}
