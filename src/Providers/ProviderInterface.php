<?php

namespace PaymentEngine\Providers;

/**
 * Interface ProviderInterface
 * @package PaymentEngine\Providers
 */
interface ProviderInterface
{
    /**
     * @return mixed
     */
    public function getName();

    /**
     * @param $message
     * @return mixed
     */
    public function charge($message);

    /**
     * @return bool
     */
    public function hasRedirect();

    // possible additional features that can be developed
    // todo: tokenize card (if we want to charge the customer more than once, for as auto-renewal when policy runs out)
    // todo: refund (if we want to automate the refund procedure, not MVP)
    // todo: transfer (if we need to pay money to 3rd parties like commisions etc)
    // todo: 3d-Secure (if fraud becomes a problem)

}
