<?php

namespace PaymentEngine\Tests;

use PaymentEngine\PaymentStrategy;
use PaymentEngine\Messages\Error;
use PaymentEngine\Messages\Success;
use PHPUnit_Framework_TestCase;
use ReflectionClass;

/**
 * Class PaymentTest
 * @package PaymentEngine\Tests
 * @author Alfonso Rodriguez <beabys@gmail.com>
 */
class PaymentTest extends PHPUnit_Framework_TestCase
{

    /**
     * @param $object
     * @param $method
     * @param $args
     * @return mixed
     */
    protected function invokeProtectedMethods($object, $method, $args = null) {
        $class = new ReflectionClass(get_class($object));
        $method = $class->getMethod($method);
        $method->setAccessible(true);
        if (is_null($args)) {
            return $method->invoke($object);
        }
        return $method->invoke($object, $args);
    }

    /**
     * Test Creation of instance for paymentStrategy
     */
    public function testIsInstanceOfPaymentStrategy()
    {
        $strategy = new PaymentStrategy();
        $this->assertInstanceOf('PaymentEngine\PaymentStrategy', $strategy, 'Instance of PaymentStrategy');
    }

    /**
     * TEST EXCEPTION CREATING INVALID PROVIDER
     */
    public function testNotExistProvider()
    {
        $this->expectException('Exception');
        $payment = new PaymentStrategy();
        $payment->build(Mocks::NOT_EXIST);
    }

    /**
     * INSTANCE OF PAYPAL
     */
    public function testPaypalInstance()
    {
        $payment = new PaymentStrategy();
        $provider = $payment->build(PaymentStrategy::PAYPAL, Mocks::$parametersConstructor);
        $this->assertInstanceOf('PaymentEngine\Providers\PaypalProvider', $provider, 'Instance of Paylpal');
    }

    /**
     * INSTANCE OF BRAINTREE
     */
    public function testBraintreeInstance()
    {
        $payment = new PaymentStrategy();
        $provider = $payment->build(PaymentStrategy::BRAINTREE, Mocks::$parametersConstructor);
        $this->assertInstanceOf('PaymentEngine\Providers\BraintreeProvider', $provider, 'Instance of Braintree');
    }

    /**
     * TEST PROVIDER RESPONSE SUCCESS RESPONSE
     */
    public function testProcessSuccess()
    {
        //Mock Success
        $success = new Success();
        $success->setCustomerId('test')
                ->setPaymentResult(Mocks::$successResultPaypal)
        ;

        //Mock Provider
        $provider = $this->getMockBuilder('PaymentEngine\Providers\PaypalProvider')
            ->disableOriginalConstructor()
            ->getMock();
        $provider->expects($this->any())
            ->method('charge')
            ->with(Mocks::$parametersCharge)
            ->will($this->returnValue($success));

        $payment = $provider;
        $paymentResponse = $payment->charge(Mocks::$parametersCharge);
        $this->assertInstanceOf('PaymentEngine\Messages\Success', $paymentResponse);
    }

    /**
     * TEST PROVIDER RESPONSE ERROR RESPONSE
     */
    public function testProcessError()
    {
        //Mock Success
        $error = new Error('invalid');
        $error
            ->setCustomerId('test')
            ->setPaymentResult(Mocks::$successResultPaypal)
        ;

        //Mock Provider
        $providerPaypal = $this->getMockBuilder('PaymentEngine\Providers\PaypalProvider')
            ->disableOriginalConstructor()
            ->getMock();
        $providerPaypal->expects($this->any())
            ->method('charge')
            ->with(Mocks::$parametersCharge)
            ->will($this->returnValue($error));
        $providerBraintree = $this->getMockBuilder('PaymentEngine\Providers\BraintreeProvider')
            ->disableOriginalConstructor()
            ->getMock();
        $providerBraintree->expects($this->any())
            ->method('charge')
            ->with(Mocks::$parametersCharge)
            ->will($this->returnValue($error));
        $payment = $providerPaypal;
        $paymentResponse = $payment->charge(Mocks::$parametersCharge);
        $this->assertInstanceOf('PaymentEngine\Messages\Error', $paymentResponse);
    }

}
